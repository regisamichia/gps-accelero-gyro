//
//  ViewController.h
//  Accelero
//
//  Created by Helene Bertolotti-Nguyen on 26/07/2017.
//  Copyright © 2017 Regis. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreMotion/CoreMotion.h>


double currentMaxX;
double currentMaxY;
double currentMaxZ;

double currentMaxRotX;
double currentMaxRotY;
double currentMaxRotZ;

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *x;
@property (weak, nonatomic) IBOutlet UILabel *y;
@property (weak, nonatomic) IBOutlet UILabel *z;

@property (weak, nonatomic) IBOutlet UILabel *mouvement;

@property (weak, nonatomic) IBOutlet UILabel *maxX;
@property (weak, nonatomic) IBOutlet UILabel *maxY;
@property (weak, nonatomic) IBOutlet UILabel *maxZ;

@property (weak, nonatomic) IBOutlet UILabel *rotX;
@property (weak, nonatomic) IBOutlet UILabel *rotY;
@property (weak, nonatomic) IBOutlet UILabel *rotZ;

@property (weak, nonatomic) IBOutlet UILabel *maxRotX;
@property (weak, nonatomic) IBOutlet UILabel *maxRotY;
@property (weak, nonatomic) IBOutlet UILabel *maxRotZ;

@property (weak, nonatomic) IBOutlet UILabel *energy;

- (IBAction)resetMaxValues:(id)sender;

@property (strong, nonatomic) CMMotionManager *motionManager;

@end

