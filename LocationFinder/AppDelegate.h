//
//  AppDelegate.h
//  LocationFinder
//
//  Created by Helene Bertolotti-Nguyen on 26/07/2017.
//  Copyright © 2017 Regis. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong) NSPersistentContainer *persistentContainer;

- (void)saveContext;


@end

