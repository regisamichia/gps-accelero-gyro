//
//  ViewController.h
//  LocationFinder
//
//  Created by Helene Bertolotti-Nguyen on 26/07/2017.
//  Copyright © 2017 Regis. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface ViewController : UIViewController <CLLocationManagerDelegate>

@property (weak, nonatomic) IBOutlet UILabel *latitudeLabel;
@property (weak, nonatomic) IBOutlet UILabel *longitudeLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *speedLabel;

- (IBAction)getCurrentLocation:(id)sender;

@end

